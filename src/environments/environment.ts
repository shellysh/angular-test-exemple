// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  url: "http://localhost/angular/testexample/slim/",
  firebase:{
    apiKey: "AIzaSyBzBPtIEhiv7RB2jozDj1OyncvgWzMLWKo",
    authDomain: "testexample-47dbc.firebaseapp.com",
    databaseURL: "https://testexample-47dbc.firebaseio.com",
    projectId: "testexample-47dbc",
    storageBucket: "testexample-47dbc.appspot.com",
    messagingSenderId: "829055869419"
  }
};
